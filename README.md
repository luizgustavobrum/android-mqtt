# Aplicação Android MQTT

Uma implementação em Android com o protocolo MQTT. Criação da Classe MQTTClient para agrupar as 3 funções: Connect, Publish e Subcribe. Usamos o "cloudmqtt" como Broker. 

## Referências

- [Biblioteca-Android](https://www.eclipse.org/paho/clients/android/)
- [Artigo-Android](https://www.hivemq.com/blog/mqtt-client-library-enyclopedia-paho-android-service/)
- [Repositório-Android](https://mvnrepository.com/artifact/org.eclipse.paho/org.eclipse.paho.client.mqttv3/1.0.2)
- [Medium-Android-NodeMCU](https://medium.com/coinmonks/real-time-data-transfer-for-iot-with-mqtt-android-and-nodemcu-ae4b01f87be4)
- [Biblioteca-Javascript](https://www.eclipse.org/paho/clients/js/)
- [MQTT-Javascript](https://www.cloudmqtt.com/docs/websocket.html)

## Adicionando a Biblioteca
Gradle em Nivel de Projeto
```
buildscript {
    repositories {
        google()
        jcenter()
        //Adicionar essas linhas
        maven {
            url "https://repo.eclipse.org/content/repositories/paho-releases/" 
        }
        //
        
    }
```

Gradle em Nivel de App
```
implementation 'org.eclipse.paho:org.eclipse.paho.client.mqttv3:1.1.0'
implementation 'org.eclipse.paho:org.eclipse.paho.android.service:1.1.1'
```

[Link](https://stackoverflow.com/questions/45166834/how-to-add-paho-mqtt-to-android-studio)