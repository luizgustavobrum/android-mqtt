/*
* Direitos Autorais: https://medium.com/coinmonks/real-time-data-transfer-for-iot-with-mqtt-android-and-nodemcu-ae4b01f87be4
*
* */
package com.example.mqttteste;
import android.content.Context;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class MQTTClient {

    private Context context;
    private String server, user, password, clientId;
    private MqttAndroidClient mqttClient;

    public MQTTClient(Context context, String server, String user, String password, String clientId){
        this.context = context;
        this.server = server;
        this.user = user;
        this.password = password;
        this.clientId = clientId;
    }

    public MqttAndroidClient connectServer(){
        mqttClient = new MqttAndroidClient(this.context, this.server, this.clientId);
        MqttConnectOptions options = new MqttConnectOptions();
        options.setUserName(this.user);
        options.setPassword(this.password.toCharArray());

        try {
            //IMqttToken token = mqttClient.connect(options);
            mqttClient.connect(options,context, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Toast.makeText(context, "Conectado", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Toast.makeText(context, "Desconectado", Toast.LENGTH_SHORT).show();
                }
            });

        } catch (MqttException e){
            e.printStackTrace();
        }
        return mqttClient;
    }

    public void publish( MqttAndroidClient client,String topic,String payload){

        try {
            MqttMessage message = new MqttMessage(payload.getBytes(StandardCharsets.UTF_8));
            client.publish(topic, message);
        } catch (MqttException e) {
            e.printStackTrace();
        }

    }

    public void subcribe(MqttAndroidClient client,String topic){
        int qos = 1;
        try {
            IMqttToken subToken = client.subscribe(topic, qos);
            subToken.setActionCallback(new IMqttActionListener() {

                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // The message was published
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    // The subscription could not be performed, maybe the user was not
                    // authorized to subscribe on the specified topic e.g. using wildcards
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }

    }
}
