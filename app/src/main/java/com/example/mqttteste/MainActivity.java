package com.example.mqttteste;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity {

    private Button button;
    private String mensagem;
    static String SERVER = "tcp://postman.cloudmqtt.com:16328"; //Seu Servidor
    static String USER = "ehxjyvmt";
    static String PASSWORD = "hquN2HsKDxng";
    MqttAndroidClient client;
    MQTTClient mqttClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.btn);
        String clientId = MqttClient.generateClientId();

        mqttClient = new MQTTClient(this.getApplicationContext(), SERVER, USER, PASSWORD, clientId);
        client = mqttClient.connectServer();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mqttClient.publish(client, "lcc", "teste");
            }
        });
    }

}
